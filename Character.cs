﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace CSSkeleton {
    internal class Character : Sprite {
        public bool Jumping { get; set; }
        public float Gravity { get; set; }
        private SoundEffect sndJump;
        private SoundEffect sndLanding;
        private int groundPosition;

        public Character (SpriteBatch pSpriteBatch, Texture2D pTexture) : base(pSpriteBatch, pTexture, 20, 20) {
            Jumping = false;
            Gravity = Settings.Gravity;
        }

        public void SetSounds (SoundEffect pSndJump, SoundEffect pSndLanding) {
            sndJump = pSndJump;
            sndLanding = pSndLanding;
        }

        public void SetFloorPosition (int pPosition) {
            Y = pPosition;
            groundPosition = pPosition;
        }

        public void Jump () {
            if (Jumping) return; // NOTE: change here to allow double-jump
            Velocity.Y = Settings.VelocityJump;
            Jumping = true;
            // Play then jump sound
            sndJump?.Play();
        }

        protected override void Update (GameTime gameTime) {
            // Test if the hero is on the ground
            if (Jumping && Y > groundPosition) {
                // fix the vertical position to the ground
                Y = groundPosition;
                // Stop jumping
                Jumping = false;
                Velocity.Y = 0;
                // Play then landing sound
                sndLanding?.Play();
            }

            // Apply velocity
            if (Jumping) {
                Velocity.Y += Gravity;
            }

            base.Update(gameTime);
        }
    }
}
