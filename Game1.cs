﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace CSSkeleton {
    // create a global instance of the game object so that its properties are accessible from anywhere
    public class MyGame : Game {
        public static MyGame Instance;

        public MyGame () {
            Instance = this;
        }
    }

    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game {
        SpriteBatch spriteBatch;
        private readonly GraphicsDeviceManager graphics;
        private KeyboardState keyboardStateOld;

        // entities
        private Map gameMap = null;
        private Character player;
        private readonly MusicManager musicManager;

        // screens and display
        private RenderTarget2D render; // NOTE: allows to change the display resolution while keeping the aspect ratio.
        private Vector2 screenSize;

        // Musics and Sounds
        private Song musicCool;
        private int musicTechnoIndex;
        private Song musicTechno;
        private int musicCoolIndex;

        public Game1 () {
            // Change screen resolution
            graphics = new GraphicsDeviceManager(this) {
                    PreferredBackBufferWidth = Settings.ScreenWidth,
                    PreferredBackBufferHeight = Settings.ScreenHeight,
                    IsFullScreen = false
            };
            graphics.ApplyChanges();

            screenSize.X = graphics.PreferredBackBufferWidth / 2.0f;
            screenSize.Y = graphics.PreferredBackBufferHeight / 2.0f;

            Content.RootDirectory = "Content";

            musicManager = new MusicManager();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize () {
            keyboardStateOld = Keyboard.GetState();

            // renderTaget initialize
            PresentationParameters pp = graphics.GraphicsDevice.PresentationParameters;
            render = new RenderTarget2D(graphics.GraphicsDevice,
                    Settings.ScreenWidth, Settings.ScreenHeight,
                    false,
                    SurfaceFormat.Color,
                    DepthFormat.None,
                    pp.MultiSampleCount,
                    RenderTargetUsage.DiscardContents
            );

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent () {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Background.LstBackgrounds.Add(new Background(Content.Load<Texture2D>("forest"), Settings.ScrollingSpeed, 0, 126));
            Background.LstBackgrounds.Add(new Background(Content.Load<Texture2D>("volcano"), Settings.ScrollingSpeed, 0, 126));

            SoundEffect sndJump = Content.Load<SoundEffect>("sfx_movement_jump13");
            SoundEffect sndLanding = Content.Load<SoundEffect>("sfx_movement_jump13_landing");
            player = new Character(spriteBatch, Content.Load<Texture2D>("herosheet"));

            player.SetSounds(sndJump, sndLanding);
            player.Velocity.X = 2;
            player.AnimationAdd("run", new[] {0, 1, 2, 3, 4, 5, 6, 7}, Settings.AnimationSpeed);
            player.AnimationPlay("run");
            player.X = screenSize.X / 4;
            player.SetFloorPosition((int)screenSize.Y + Settings.FloorOffset);

            // Load musics and sounds
            musicCool = Content.Load<Song>("cool");
            musicTechno = Content.Load<Song>("techno");
            musicCoolIndex = musicManager.AddTrack(musicCool);
            musicTechnoIndex = musicManager.AddTrack(musicTechno);
            musicManager.PlayTrack(musicCoolIndex);

            // Load the gameMap
            // NOTE: content and tiled files must be added to the project to use the map
            // see the tileMap projet to see a full implementation example
            /*
            gameMap = new Map(Content, "Content/map.tmx");
            */
        }

        /*
        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent() {
          // Unload any non ContentManager content here
        }
        */
        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="pGameTime">Provides a snapshot of timing values.</param>
        protected override void Update (GameTime pGameTime) {
            // Get a new keyboard state to get the player input or compare with the old one...
            KeyboardState keyboardState = Keyboard.GetState();

            musicManager.Update(pGameTime);

            Background.UpdateAll(pGameTime);

            Sprite.UpdateAll(pGameTime);

            // keyboard Keys for the App
            // ******************************
            // Quit game
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || keyboardState.IsKeyDown(Settings.AppKeys["Quit"])) {
                Exit();
                //Environment.Exit(0);
            }

            // Toggle FullScreen
            if (keyboardState.IsKeyDown(Settings.AppKeys["FullScreen"]) && !keyboardStateOld.IsKeyDown(Settings.AppKeys["FullScreen"])) {
                if (!graphics.IsFullScreen) {
                    graphics.PreferredBackBufferWidth = graphics.GraphicsDevice.DisplayMode.Width;
                    graphics.PreferredBackBufferHeight = graphics.GraphicsDevice.DisplayMode.Height;
                } else {
                    graphics.PreferredBackBufferWidth = Settings.ScreenWidth;
                    graphics.PreferredBackBufferHeight = Settings.ScreenHeight;
                }

                graphics.ToggleFullScreen();
            }

            // keyboard Keys for the player
            // ******************************
            // jump
            if (keyboardState.IsKeyDown(Settings.PlayerKeys["Jump"]) && !keyboardStateOld.IsKeyDown(Settings.PlayerKeys["Jump"])) {
                Debug.WriteLine("Jump !");
                player.Jump();
            }

            // move the player left
            if (keyboardState.IsKeyDown(Settings.PlayerKeys["Left"]) && player.X > 0) {
                player.X -= player.Velocity.X;
            }

            // move the player right
            if (keyboardState.IsKeyDown(Settings.PlayerKeys["Right"]) && player.X < screenSize.X) {
                player.X += player.Velocity.X;
            }

            // Select the music depending of the player position
            musicManager.PlayTrack(player.X > screenSize.X / 2 ? musicTechnoIndex : musicCoolIndex);

            keyboardStateOld = keyboardState;
            base.Update(pGameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="pGameTime">Provides a snapshot of timing values.</param>
        protected override void Draw (GameTime pGameTime) {
            // START of the graphic rendering
            // -------------------
            GraphicsDevice.SetRenderTarget(render);

            GraphicsDevice.Clear(Color.Beige);

            spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, null, null, null, Matrix.CreateScale(2));

            // START OF CODE FOR DEMO ONLY
            // -------------------
            // Draw the backgroud according to the player position
            if (player.X > screenSize.X / 2) {
                Background.LstBackgrounds[1].Draw(spriteBatch);
            } else {
                Background.LstBackgrounds[0].Draw(spriteBatch);
            }
            // END OF CODE FOR DEMO ONLY
            // -------------------

            if (gameMap != null) gameMap.Draw(spriteBatch);

            Sprite.DrawAll(pGameTime);

            spriteBatch.End();

            GraphicsDevice.SetRenderTarget(null);
            // END of the graphic rendering
            // -------------------

            // START of the render drawing
            // -------------------
            float ratio = 1;
            var marginV = 0;
            var marginH = 0;
            var currentAspect = Window.ClientBounds.Width / (float)Window.ClientBounds.Height;
            float virtualAspect = Settings.ScreenWidth / (float)Settings.ScreenHeight;
            if (Settings.ScreenHeight != Window.ClientBounds.Height) {
                if (currentAspect > virtualAspect) {
                    ratio = Window.ClientBounds.Height / (float)Settings.ScreenHeight;
                    marginH = (int)((Window.ClientBounds.Width - Settings.ScreenWidth * ratio) / 2);
                } else {
                    ratio = Window.ClientBounds.Width / (float)Settings.ScreenWidth;
                    marginV = (int)((Window.ClientBounds.Height - Settings.ScreenHeight * ratio) / 2);
                }
            }

            Rectangle destinationRectangle = new Rectangle(marginH, marginV, (int)(Settings.ScreenWidth * ratio), (int)(Settings.ScreenHeight * ratio));

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (Settings.ScreenBlur)
                spriteBatch.Begin();
            else
                spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointClamp);

            spriteBatch.Draw(render, destinationRectangle, Color.White);

            spriteBatch.End();

            // END of the render drawing
            // -------------------

            base.Draw(pGameTime);
        }
    }
}
