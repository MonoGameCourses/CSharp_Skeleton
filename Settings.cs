﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;

namespace CSSkeleton {
    internal static class Settings {
        // display
        public static int ScreenWidth = 1024;
        public static int ScreenHeight = 768;
        public static bool ScreenBlur = false;

        // gameplay and player
        public static float AnimationSpeed = 1f / 12f;
        public static float VelocityJump = -200f;
        public static float Gravity = 10f;
        public static float DefautSpeed = 60f;
        public static float VolumeStep = 0.01f;
        public static int FloorOffset = -25;
        public static float ScrollingSpeed = -2f;

        // keys settings
        public static Dictionary<string, Keys> AppKeys = new Dictionary<string, Keys>();
        public static Dictionary<string, Keys> PlayerKeys = new Dictionary<string, Keys>();

        static Settings () {
            AppKeys.Add("FullScreen", Keys.F);
            AppKeys.Add("Quit", Keys.Escape);
            PlayerKeys.Add("Jump", Keys.Up);
            PlayerKeys.Add("Right", Keys.Right);
            PlayerKeys.Add("Left", Keys.Left);
        }
    }
}
