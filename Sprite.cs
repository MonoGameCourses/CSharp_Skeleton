﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CSSkeleton {
    internal class Sprite {
        public Dictionary<string, string> Properties { get; }
        public SpriteBatch SpriteBatch { get; }

        public Texture2D Texture { get; }

        public float X { get; set; }
        public float Y { get; set; }
        public Vector2 Velocity;
        public float Speed { get; set; }

        public float Rotation { get; set; }
        public float Scale { get; set; }

        public readonly List<Animation> Animations;
        public Animation CurrentAnimation;
        public int Frame { get; }
        public int FrameWidth { get; private set;}
        public int FrameHeight { get; private set;}
        public float AnimationTimer;

        public bool IsVisible { get; set; }
        public bool IsCentered { get; set; }

        public static readonly List<Sprite> LstSprites = new List<Sprite>();

        protected SpriteEffects Effect;

        public static void DrawAll (GameTime gametime) {
            foreach (var sprite in LstSprites) {
                sprite.Draw(gametime);
            }
        }

        public static void UpdateAll (GameTime gametime) {
            foreach (var sprite in LstSprites) {
                sprite.Update(gametime);
            }
        }

        public Sprite (SpriteBatch pSpriteBatch, Texture2D pTexture, int pFrameWidth, int pFrameHeight) {
            SpriteBatch = pSpriteBatch;
            Texture = pTexture;
            IsVisible = true;
            IsCentered = true;
            Frame = 0;
            FrameWidth = pFrameWidth;
            FrameHeight = pFrameHeight;
            Scale = 1.0f;
            Speed = Settings.DefautSpeed;
            Effect = SpriteEffects.None;
            Animations = new List<Animation>();
            LstSprites.Add(this);
            Properties = new Dictionary<string, string>();
            Velocity = Vector2.Zero;
        }

        public string GetProperty (string pName) {
            return Properties.ContainsKey(pName) ? Properties[pName] : "";
        }

        protected virtual void Update (GameTime gameTime) {
            X += Velocity.X * (float)gameTime.ElapsedGameTime.TotalSeconds;
            Y += Velocity.Y * (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Traitement des animations image par image
            if (CurrentAnimation == null) return;
            if (Math.Abs(CurrentAnimation.FrameLength) < .01f) return;
            if (CurrentAnimation.IsFinished) return;

            AnimationTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (AnimationTimer > CurrentAnimation.FrameLength) {
                //Debug.WriteLine("Sprite/Update: Changement de frame, courante={0}", frame);
                Frame++;
                if (Frame >= CurrentAnimation.Frames.Length) {
                    if (CurrentAnimation.IsLooping) {
                        Frame = 0;
                    } else {
                        Frame--;
                        CurrentAnimation.IsFinished = true;
                    }
                }

                AnimationTimer = 0;
            }
        }

        public virtual void Draw (GameTime pGameTime) {
            if (!IsVisible) return;

            var source = new Rectangle(CurrentAnimation.Frames[Frame] * FrameWidth, 0, FrameWidth, FrameHeight);
            var origine = new Vector2(0, 0);

            if (IsCentered) {
                origine = new Vector2(FrameWidth / 2.0f, FrameHeight / 2.0f);
            }

            var position = new Vector2(X, Y);

            SpriteBatch.Draw(Texture, position, source, Color.White, Rotation, origine, Scale, Effect, 0.0f);
        }

        public void AnimationAdd (string pName, int[] pFrames, float pDureeFrame, bool pisLoop = true) {
            var animation = new Animation(pName, pFrames, pDureeFrame, pisLoop);
            Animations.Add(animation);
        }

        public void AnimationPlay (string pName) {
            Debug.WriteLine("AnimationPlay({0})", pName);
            foreach (Animation element in Animations) {
                if (element.Name == pName) {
                    CurrentAnimation = element;
                    Frame = 0;
                    CurrentAnimation.IsFinished = false;
                    Debug.WriteLine("AnimationPlay, OK {0}", CurrentAnimation.Name);
                    break;
                }
            }

            Debug.Assert(CurrentAnimation != null, "AnimationPlay : Aucune animation trouvée");
        }
    }
}
