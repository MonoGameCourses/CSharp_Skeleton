﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace CSSkeleton {
    internal class MusicManager {
        public int CurrentTrackIndex;
        public int NextTrackIndex;
        public readonly List<Song> LstTracks;

        public MusicManager () {
            LstTracks = new List<Song>();
            CurrentTrackIndex = -1;
            MediaPlayer.Volume = 0;
        }

        public int AddTrack (Song pSong) {
            LstTracks.Add(pSong);
            return LstTracks.Count - 1;
        }

        public void Update (GameTime pGameTime) {
            if (CurrentTrackIndex != NextTrackIndex) {
                MediaPlayer.Volume -= Settings.VolumeStep;
                if (MediaPlayer.Volume <= 0f) {
                    MediaPlayer.Volume = 0f;
                    var mySong = LstTracks[NextTrackIndex];

                    MediaPlayer.IsRepeating = true;
                    MediaPlayer.Play(mySong);
                    CurrentTrackIndex = NextTrackIndex;
                    Debug.WriteLine("Start playing music " + NextTrackIndex);
                }
            } else if (CurrentTrackIndex == NextTrackIndex && MediaPlayer.Volume < 1) {
                try {
                    // NOTE: if the volume changes too fast, the media player may be considered as NULL (monogame bug ?)
                    MediaPlayer.Volume += Settings.VolumeStep;
                } catch (NullReferenceException) {
                    Debug.WriteLine("Mediaplayer is NULL !");
                }
            }
        }

        public void PlayTrack (int pTrackIndex) {
            if (NextTrackIndex == pTrackIndex) return;
            NextTrackIndex = pTrackIndex;
            Debug.WriteLine("Change music from {0} to {1}", CurrentTrackIndex, NextTrackIndex);
        }
    }
}
