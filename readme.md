## CSharp_Skeleton

### Description  
Framework C# reposant sur monogame, regroupant des fonctionnalités utiles à un jeu vidéo.  

===
### Objectifs  
Aucun. 

### Mouvements et actions du joueur  
Déplacer le joueur: touches ZQSD ou bien les flèches du clavier.   
Quitter le jeu: clic sur la croix ou touche escape.  

### Interactions  
Aucun. 

### Copyrights  
Ecrit en C# (version 6.0 et plus) et en utilisant le Framework MonoGame (version 3.5 et +).  
Développé en utilisant principalement Visual Studio 2017 et JetBrain Rider 2018.  

-----------------
(C) 2019 GameMeaMea Studio (http://www.gamemeamea.com)

=================================================

### Description  
C# framework based on monogame, gathering useful features for a video game.  

===
### Goals  
None.  

### Movements and actions of the player  
Move the player: ZQSD keys or the arrows on the keyboard.  
Exit the game: click on the cross or escape key.  

### Interactions  
None.  

### Copyrights  
Written in C# (6.0 version and more) using the Monogame Framework (3.5 version and more).  
Developed mainly using Visual Studio 2017 and JetBrain Rider 2018.    

------------------
(C) 2019 GameMeaMea Studio (http://www.gamemeamea.com)
