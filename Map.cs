﻿using System;
using System.IO;
using TiledSharp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace CSSkeleton {
    internal class Map {
        // ReSharper disable once NotAccessedField.Local
        private ContentManager pContent;

        // map
        public TmxMap MapSource { get; }
        public Texture2D Tileset { get; }
        public int Cols { get; }
        public int Rows { get; }
        public int TileWidth { get; }
        public int TileHeight { get; }
        public int TileSetCols { get; }
        public int TileSetRows { get; }

        public Map (ContentManager pContent, string pMapFileName, int pTileSetIndex = 0) {
            MapSource = new TmxMap(pMapFileName);
            this.pContent = pContent;
            if (MapSource.Tilesets?[pTileSetIndex] == null) {
                return;
            }

            var tileSetFileName = Path.GetFileNameWithoutExtension(MapSource.Tilesets[pTileSetIndex].Image.Source);
            Console.WriteLine("Loading TILESET:" + tileSetFileName);
            Tileset = pContent.Load<Texture2D>(tileSetFileName);

            Cols = MapSource.Width;
            Rows = MapSource.Height;

            TileWidth = MapSource.Tilesets[pTileSetIndex].TileWidth;
            TileHeight = MapSource.Tilesets[pTileSetIndex].TileHeight;

            var imageWidth = MapSource.Tilesets[pTileSetIndex].Image.Width;
            if (imageWidth != null) TileSetCols = (int)imageWidth / TileWidth;

            var imageHeight = MapSource.Tilesets[pTileSetIndex].Image.Height;
            if (imageHeight != null) TileSetRows = (int)imageHeight / TileHeight;
        }

        public void Draw (SpriteBatch pSpriteBatch, int pOffsetX = 0, int pOffsetY = 0) {
            foreach (var layer in MapSource.Layers) {
                var row = 0;
                var col = 0;
                foreach (var tile in layer.Tiles) {
                    var tileId = tile.Gid;
                    if (tileId != 0) {
                        var tileFrame = tileId - 1; // NOTE: the indexes in the description of the map (.tmx file) and in the tileset are shifted by 1.
                        var tileSetCol = tileFrame % TileSetCols;
                        var tileSetRow = tileFrame / TileSetCols;
                        var drawX = col * TileWidth + pOffsetX;
                        var drawY = row * TileHeight + pOffsetY;
                        var tileSetRectangle = new Rectangle(tileSetCol * TileWidth, tileSetRow * TileHeight, TileWidth, TileHeight);
                        pSpriteBatch.Draw(Tileset, new Vector2(drawX, drawY), tileSetRectangle, Color.White);
                    }

                    col++;
                    if (col < Cols) {
                        continue;
                    }

                    row++;
                    col = 0;
                }
            }
        }
    }
}
