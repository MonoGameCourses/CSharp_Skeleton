﻿namespace CSSkeleton {
    internal class Animation {
        public string Name { get; }
        public int[] Frames { get; }
        public float FrameLength { get; }
        public bool IsLooping { get; set; }
        public bool IsFinished { get; set; }

        public Animation (string pName, int[] pFrames, float pTime, bool pisLooping) {
            Name = pName;
            Frames = pFrames;
            FrameLength = pTime;
            IsLooping = pisLooping;
            IsFinished = false;
        }

        public Animation (string pName, int[] pFrames) : this(pName, pFrames, Settings.AnimationSpeed, true) { }
    }
}
