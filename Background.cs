﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CSSkeleton {
    internal class Background {
        public Texture2D Image { get; set; }
        public float Speed { get; set; }
        public Vector2 Position { get; set; }

        public static readonly List<Background> LstBackgrounds = new List<Background>();

        public static void UpdateAll (GameTime pGametime) {
            foreach (var background in LstBackgrounds) {
                background.Update(pGametime);
            }
        }

        public Background (Texture2D pImage, float pSpeed, int pPositionX = 0, int pPositionY = 0) {
            Image = pImage;
            Speed = pSpeed;
            Position = new Vector2(pPositionX, pPositionY);
        }

        public void Update (GameTime pGametime) {
            var position = new Vector2();
            position = Position;
            position.X += Speed * (float)pGametime.ElapsedGameTime.TotalSeconds;
            if (position.X <= 0 - Image.Width)
                position.X += Image.Width;
            Position = position;
        }

        public void Draw (SpriteBatch pSpriteBatch) {
            pSpriteBatch.Draw(Image, Position, Color.White);
            if (Position.X < 0)
                pSpriteBatch.Draw(Image, new Vector2(Position.X + Image.Width, Position.Y), Color.White);
        }
    }
}
